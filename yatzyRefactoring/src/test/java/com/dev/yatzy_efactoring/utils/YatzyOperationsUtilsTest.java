package com.dev.yatzy_efactoring.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(YatzyOperationsUtils.class)
@ComponentScan({"com.dev.yatzyRefactoring"})
public class YatzyOperationsUtilsTest {

    @Test
    public void should_ReturnListOfElements_when_PairsExist() {
        List<Integer> pairsList1 = YatzyOperationsUtils.retrievePairs(1,1,3,3,6, 2);
        List<Integer> pairsList2 = YatzyOperationsUtils.retrievePairs(1,2,3,3,6, 2);
        Assert.assertEquals(2, pairsList1.size());
        Assert.assertEquals(1, pairsList2.size());
    }

    @Test
    public void should_EmptyList_when_NoPairsExist() {
        List<Integer> pairsList = YatzyOperationsUtils.retrievePairs(1,2,4,3,6, 2);
        Assert.assertEquals(0, pairsList.size());
    }

    @Test
    public void should_ReturnMapOfElementsWithFrequency() {
        Map<Integer, Integer> frequencyMap1 = YatzyOperationsUtils.getElementsFrequency(1,1,1,1,1);
        Map<Integer, Integer> frequencyMap2 = YatzyOperationsUtils.getElementsFrequency(1,2,2,2,2);
        Map<Integer, Integer> frequencyMap3 = YatzyOperationsUtils.getElementsFrequency(1,2,3,4,6);
        Assert.assertEquals(1, frequencyMap1.size());
        Assert.assertEquals(2, frequencyMap2.size());
        Assert.assertEquals(5, frequencyMap3.size());
    }

    @Test
    public void should_ReturnTrue_when_DifferenceBetweenAllElementsEqualToOne() {
        List<Integer> diceElements = Arrays.asList(2, 1, 3, 5, 4);
        List<Integer> sortedList = diceElements.stream().sorted().collect(Collectors.toList());
        boolean isDifferenceEqualToOne = YatzyOperationsUtils.isDifferenceEqualToOneWhenSortedList(sortedList);
        Assert.assertTrue(isDifferenceEqualToOne);
    }

    @Test
    public void should_ReturnFalse_when_DifferenceBetweenAllElementsNotEqualToOne() {
        List<Integer> diceElements = Arrays.asList(2, 1, 6, 5, 4);
        List<Integer> sortedList = diceElements.stream().sorted().collect(Collectors.toList());
        boolean isDifferenceEqualToOne = YatzyOperationsUtils.isDifferenceEqualToOneWhenSortedList(sortedList);
        Assert.assertFalse(isDifferenceEqualToOne);
    }

}
