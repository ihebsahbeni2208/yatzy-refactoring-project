package com.dev.yatzy_efactoring.controller;

import com.dev.yatzy_efactoring.enums.CategoryEnum;
import com.dev.yatzy_efactoring.service.YatzyService;
import com.dev.yatzy_efactoring.service.impl.YatzyServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc

public class YatzyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    YatzyService yatzyService;

    @InjectMocks
    YatzyServiceImpl yatzyServiceImpl;
    @Test
    public void getScoreOfRound() throws Exception {
        Mockito.when(yatzyService.getScore(1,2,3,4,5, CategoryEnum.CHANCE)).thenReturn(15);
        ResultActions response = mockMvc.perform(get("/yatzy/rounds"))
                .andExpect(content().contentType("application/json"))
                .andDo(print()).andExpect(status().isOk());
    }
}
