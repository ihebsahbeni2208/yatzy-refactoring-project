package com.dev.yatzy_efactoring.service;

import com.dev.yatzy_efactoring.enums.CategoryEnum;
import com.dev.yatzy_efactoring.service.impl.YatzyServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(YatzyService.class)
@ComponentScan({"com.dev.yatzyRefactoring"})
public class YatzyServiceTest {

    @InjectMocks
    YatzyServiceImpl yatzyServiceImpl;

    @Test
    public void should_SumAllDice_when_CategoryIsChance() {
        int score1 = yatzyServiceImpl.getScore(1,1,3,3,6, CategoryEnum.CHANCE);
        int score2 = yatzyServiceImpl.getScore(4,5,5,6,1, CategoryEnum.CHANCE);
        Assert.assertEquals(14, score1);
        Assert.assertEquals(21, score2);
    }

    @Test
    public void should_Return50_when_CategoryIsYatzy_and_AllDiceNumbersAreTheSame() {
        int score = yatzyServiceImpl.getScore(1,1,1,1,1, CategoryEnum.YATZY);
        Assert.assertEquals(50, score);
    }

    @Test
    public void should_Return0_when_CategoryIsYatzy_and_AllDiceNumbersAreNotTheSame() {
        int score = yatzyServiceImpl.getScore(1,1,1,2,1, CategoryEnum.YATZY);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfDice_when_CategoryIsOnes_and_Dice1Exists() {
        int score = yatzyServiceImpl.getScore(1,1,5,3,6,CategoryEnum.ONES);
        Assert.assertEquals(2, score);
    }

    @Test
    public void should_Return0_when_CategoryIsOnes_and_Dice1DoesntExists() {
        int score = yatzyServiceImpl.getScore(3,3,3,4,5,CategoryEnum.ONES);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfDice_when_CategoryIsTwos_and_Dice2Exists() {
        int score = yatzyServiceImpl.getScore(2,3,2,5,1,CategoryEnum.TWOS);
        Assert.assertEquals(4, score);
    }

    @Test
    public void should_Return0_when_CategoryIsTwos_and_Dice2DoesntExists() {
        int score = yatzyServiceImpl.getScore(3,4,5,3,6,CategoryEnum.TWOS);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfDice_when_CategoryIsThrees_and_Dice3Exists() {
        int score = yatzyServiceImpl.getScore(2,3,2,5,1,CategoryEnum.THREES);
        Assert.assertEquals(3, score);
    }

    @Test
    public void should_Return0_when_CategoryIsThrees_and_Dice3DoesntExists() {
        int score = yatzyServiceImpl.getScore(2,4,5,1,6,CategoryEnum.THREES);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfDice_when_CategoryIsFours_and_Dice4Exists() {
        int score = yatzyServiceImpl.getScore(1,1,2,4,4,CategoryEnum.FOURS);
        Assert.assertEquals(8, score);
    }

    @Test
    public void should_Return0_when_CategoryIsFours_and_Dice4DoesntExists() {
        int score = yatzyServiceImpl.getScore(2,2,5,1,6,CategoryEnum.FOURS);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfDice_when_CategoryIsFives_and_Dice5Exists() {
        int score = yatzyServiceImpl.getScore(1,1,2,5,5,CategoryEnum.FIVES);
        Assert.assertEquals(10, score);
    }

    @Test
    public void should_Return0_when_CategoryIsFives_and_Dice5DoesntExists() {
        int score = yatzyServiceImpl.getScore(2,2,1,1,6,CategoryEnum.FIVES);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfDice_when_CategoryIsSixes_and_Dice6Exists() {
        int score = yatzyServiceImpl.getScore(6,6,6,6,6,CategoryEnum.SIXES);
        Assert.assertEquals(30, score);
    }

    @Test
    public void should_Return0_when_CategoryIsSixes_and_Dice6DoesntExists() {
        int score = yatzyServiceImpl.getScore(2,2,5,1,1,CategoryEnum.SIXES);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfMaxPairs_when_CategoryIsPair() {
        int score1 = yatzyServiceImpl.getScore(3,3,3,4,4, CategoryEnum.PAIR);
        int score2 = yatzyServiceImpl.getScore(1,1,6,2,6, CategoryEnum.PAIR);
        int score3 = yatzyServiceImpl.getScore(3,3,3,4,1, CategoryEnum.PAIR);
        int score4 = yatzyServiceImpl.getScore(3,3,3,3,1, CategoryEnum.PAIR);
        Assert.assertEquals(8, score1);
        Assert.assertEquals(12, score2);
        Assert.assertEquals(6, score3);
        Assert.assertEquals(6, score4);
    }

    @Test
    public void should_Return0_when_CategoryIsPair_and_NoPairsFound() {
        int score = yatzyServiceImpl.getScore(1,2,3,4,5, CategoryEnum.PAIR);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOfAllPairs_when_CategoryIsTwoPairs() {
        int score1 = yatzyServiceImpl.getScore(1,1,2,3,3, CategoryEnum.TWO_PAIRS);
        int score2 = yatzyServiceImpl.getScore(1,1,2,2,2, CategoryEnum.TWO_PAIRS);
        Assert.assertEquals(8, score1);
        Assert.assertEquals(6, score2);
    }

    @Test
    public void should_Return0_when_CategoryIsTwoPairs_and_No2PairsFound() {
        int score1 = yatzyServiceImpl.getScore(1,1,2,3,4, CategoryEnum.TWO_PAIRS);
        int score2 = yatzyServiceImpl.getScore(3,3,3,3,1, CategoryEnum.TWO_PAIRS);
        Assert.assertEquals(0, score1);
        Assert.assertEquals(0, score2);
    }

    @Test
    public void should_ReturnSumOf3SameDice_when_CategoryIsThreeOfKind() {
        int score1 = yatzyServiceImpl.getScore(3,3,3,4,5,CategoryEnum.THREE_OF_KIND);
        int score2 = yatzyServiceImpl.getScore(3,3,3,3,1,CategoryEnum.THREE_OF_KIND);
        Assert.assertEquals(9, score1);
        Assert.assertEquals(9, score2);
    }

    @Test
    public void should_Return0_when_CategoryIsThreeOfKind_and_NoSameDiceFound3Times() {
        int score = yatzyServiceImpl.getScore(3,3,4,5,6,CategoryEnum.THREE_OF_KIND);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSumOf4SameDice_when_CategoryIsFourOfKind() {
        int score1 = yatzyServiceImpl.getScore(2,2,2,2,5,CategoryEnum.FOUR_OF_KIND);
        int score2 = yatzyServiceImpl.getScore(2,2,2,2,2,CategoryEnum.FOUR_OF_KIND);
        Assert.assertEquals(8, score1);
        Assert.assertEquals(8, score2);
    }

    @Test
    public void should_Return0_when_CategoryIsFourOfKind_and_NoSameDiceFound4Times() {
        int score = yatzyServiceImpl.getScore(2,2,2,5,5,CategoryEnum.FOUR_OF_KIND);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSum_when_CategoryIsSmallStraight_and_ConsecutifDiceFrom1To5() {
        int score = yatzyServiceImpl.getScore(1,2,3,4,5, CategoryEnum.SMALL_STRAIGHT);
        Assert.assertEquals(15, score);
    }

    @Test
    public void should_Return0_when_CategoryIsSmallStraight_and_NoConsecutifDice() {
        int score = yatzyServiceImpl.getScore(1,2,2,5,2, CategoryEnum.SMALL_STRAIGHT);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSum_when_CategoryIsLargeStraight_and_ConsecutifDiceFrom2To6() {
        int score = yatzyServiceImpl.getScore(2,3,4,5,6, CategoryEnum.LARGE_STRAIGHT);
        Assert.assertEquals(20, score);
    }

    @Test
    public void should_Return0_when_CategoryIsLargeStraight_and_NoConsecutifDice() {
        int score = yatzyServiceImpl.getScore(1,2,3,4,5, CategoryEnum.LARGE_STRAIGHT);
        Assert.assertEquals(0, score);
    }

    @Test
    public void should_ReturnSum_when_CategoryIsFullHouse_and_TwoOfKind_and_ThreeOfKind() {
        int score = yatzyServiceImpl.getScore(1,1,2,2,2, CategoryEnum.FULL_HOUSE);
        Assert.assertEquals(8, score);
    }

    @Test
    public void should_Return0_when_CategoryIsFullHouse_and_NotTwoOfKind_or_ThreeOfKind() {
        int score1 = yatzyServiceImpl.getScore(2,2,3,3,4, CategoryEnum.FULL_HOUSE);
        int score2 = yatzyServiceImpl.getScore(4,4,4,4,4, CategoryEnum.FULL_HOUSE);
        Assert.assertEquals(0, score1);
        Assert.assertEquals(0, score2);
    }

}
