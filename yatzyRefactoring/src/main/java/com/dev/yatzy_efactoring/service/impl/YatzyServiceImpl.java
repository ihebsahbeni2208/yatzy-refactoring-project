package com.dev.yatzy_efactoring.service.impl;

import com.dev.yatzy_efactoring.enums.CategoryEnum;
import com.dev.yatzy_efactoring.service.YatzyService;
import com.dev.yatzy_efactoring.utils.YatzyOperationsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by isahbeni on 6/3/2022.
 */
@Service
@Slf4j
public class YatzyServiceImpl implements YatzyService {

    @Override
    public int getScore(int d1, int d2, int d3, int d4, int d5, CategoryEnum category) {
        switch(category){
            case CHANCE:
                log.info("You have chosen chance category");
                return scoreWhenChance(d1,d2,d3,d4,d5);
            case YATZY:
                log.info("You have chosen chance yatzy");
                return scoreWhenYatzy(d1,d2,d3,d4,d5);
            case ONES:
                log.info("You have chosen ones category");
                return scoreWhenNumbersCategories(d1,d2,d3,d4,d5, 1);
            case TWOS:
                log.info("You have chosen twos category");
                return scoreWhenNumbersCategories(d1,d2,d3,d4,d5, 2);
            case THREES:
                log.info("You have chosen threes category");
                return scoreWhenNumbersCategories(d1,d2,d3,d4,d5, 3);
            case FOURS:
                log.info("You have chosen fours category");
                return scoreWhenNumbersCategories(d1,d2,d3,d4,d5, 4);
            case FIVES:
                log.info("You have chosen fives category");
                return scoreWhenNumbersCategories(d1,d2,d3,d4,d5, 5);
            case SIXES:
                log.info("You have chosen sixes category");
                return scoreWhenNumbersCategories(d1,d2,d3,d4,d5, 6);
            case PAIR:
                log.info("You have chosen pair category");
                return scoreWhenPair(d1,d2,d3,d4,d5);
            case TWO_PAIRS:
                log.info("You have chosen two pairs category");
                return scoreWhenTwoPairs(d1,d2,d3,d4,d5);
            case THREE_OF_KIND:
                log.info("You have chosen three of kind category");
                return scoreWhenNumberOfKind(d1,d2,d3,d4,d5,3);
            case FOUR_OF_KIND:
                log.info("You have chosen four of kind category");
                return scoreWhenNumberOfKind(d1,d2,d3,d4,d5, 4);
            case SMALL_STRAIGHT:
                log.info("You have chosen small straight category");
                return scoreWhenSmallStraight(d1,d2,d3,d4,d5);
            case LARGE_STRAIGHT:
                log.info("You have chosen large straight category");
                return scoreWhenLargeStraight(d1,d2,d3,d4,d5);
            case FULL_HOUSE:
                log.info("You have chosen full house category");
                return scoreWhenFullHouse(d1,d2,d3,d4,d5);
            default:
                log.info("Category is incorrect");
                break;
        }
        return 0;
    }
    private int scoreWhenChance(int d1, int d2, int d3, int d4, int d5) {
        List<Integer> rollDiceResult = Arrays.asList(d1, d2, d3, d4, d5);
        return rollDiceResult
                .stream()
                .reduce(0, Integer::sum);
    }

    private int scoreWhenYatzy(int... dice) {
        Set<Integer> diceToSet = Stream.of(dice)
                .flatMapToInt(Arrays::stream) //returns an IntStream
                .boxed()
                .collect(Collectors.toSet());
        final boolean allEqual = diceToSet.size() <= 1;
        return allEqual ? 50 : 0;
    }

    private int scoreWhenNumbersCategories(int d1, int d2, int d3, int d4, int d5, int categoryId) {
        return (int) Stream
                .of(d1, d2, d3, d4, d5)
                .filter(dice -> dice == categoryId)
                .count() * categoryId;
    }

    private int scoreWhenPair(int d1, int d2, int d3, int d4, int d5) {
        List<Integer> pairsList = YatzyOperationsUtils.retrievePairs(d1, d2, d3, d4, d5, 2);

        return !pairsList.isEmpty() ? pairsList
                .stream()
                .max(Integer::compare)
                .map(dice -> dice * 2).orElse(null) : 0;

    }

    private int scoreWhenTwoPairs(int d1, int d2, int d3, int d4, int d5) {
        List<Integer> pairsList = YatzyOperationsUtils.retrievePairs(d1, d2, d3, d4, d5, 2);
        return pairsList.size() == 2 ? pairsList
                .stream()
                .reduce(0, Integer::sum) * 2 : 0;
    }

    private int scoreWhenNumberOfKind(int d1, int d2, int d3, int d4, int d5, int numberOfKind) {
        List<Integer> pairsList = YatzyOperationsUtils.retrievePairs(d1, d2, d3, d4, d5, numberOfKind);
        return !pairsList.isEmpty() ? pairsList
                .stream()
                .findFirst()
                .map(dice -> dice * numberOfKind).orElse(null) : 0;
    }

    private int scoreWhenSmallStraight(int d1, int d2, int d3, int d4, int d5) {
        List<Integer> diceElements = Stream
                .of(d1, d2, d3, d4, d5)
                .sorted()
                .collect(Collectors.toList());
        return (YatzyOperationsUtils.isDifferenceEqualToOneWhenSortedList(diceElements) && diceElements.get(0) == 1) ? 15 : 0;
    }

    private int scoreWhenLargeStraight(int d1, int d2, int d3, int d4, int d5) {
        List<Integer> diceElements = Stream
                .of(d1, d2, d3, d4, d5)
                .sorted()
                .collect(Collectors.toList());
        return (YatzyOperationsUtils.isDifferenceEqualToOneWhenSortedList(diceElements) && diceElements.get(0) == 2) ? 20 : 0;
    }

    private int scoreWhenFullHouse(int d1, int d2, int d3, int d4, int d5) {
        Map<Integer, Integer> diceMap = YatzyOperationsUtils.getElementsFrequency(d1, d2, d3, d4, d5);
        if (diceMap.size() == 2 && diceMap.values().contains(2)) {
            return scoreWhenChance(d1, d2, d3, d4, d5);
        }
        return 0;
    }
}
