package com.dev.yatzy_efactoring.service;

import com.dev.yatzy_efactoring.enums.CategoryEnum;

/**
 * Created by isahbeni on 6/3/2022.
 */
public interface YatzyService {
    int getScore(int d1, int d2, int d3, int d4, int d5, CategoryEnum category);
}
