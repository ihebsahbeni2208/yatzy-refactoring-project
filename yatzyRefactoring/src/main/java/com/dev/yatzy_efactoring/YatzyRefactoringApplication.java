package com.dev.yatzy_efactoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YatzyRefactoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(YatzyRefactoringApplication.class, args);
	}

}
