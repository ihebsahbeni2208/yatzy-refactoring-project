package com.dev.yatzy_efactoring.enums;

public enum CategoryEnum {
    ONES, TWOS, THREES, FOURS, FIVES, SIXES, CHANCE, YATZY, PAIR, TWO_PAIRS, THREE_OF_KIND, FOUR_OF_KIND, SMALL_STRAIGHT, LARGE_STRAIGHT, FULL_HOUSE;
}
