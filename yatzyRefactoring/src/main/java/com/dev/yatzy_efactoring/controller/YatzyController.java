package com.dev.yatzy_efactoring.controller;

import com.dev.yatzy_efactoring.enums.CategoryEnum;
import com.dev.yatzy_efactoring.service.YatzyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by isahbeni on 6/3/2022.
 */
@RestController
@RequestMapping("/yatzy")
public class YatzyController {
    @Autowired
    YatzyService yatzyService;

    /**
     * Here you have the choice to choose the category and the dice numbers.
     */
    @GetMapping("/rounds")
    public int yatzyRounds() {
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.CHANCE);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.YATZY);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.ONES);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.TWOS);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.THREES);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.FOURS);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.FIVES);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.SIXES);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.PAIR);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.TWO_PAIRS);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.THREE_OF_KIND);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.FOUR_OF_KIND);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.SMALL_STRAIGHT);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.LARGE_STRAIGHT);
        yatzyService.getScore(1,2,3,4,5, CategoryEnum.FULL_HOUSE);

        return yatzyService.getScore(1,2,3,3,3, CategoryEnum.THREE_OF_KIND);
    }
}
