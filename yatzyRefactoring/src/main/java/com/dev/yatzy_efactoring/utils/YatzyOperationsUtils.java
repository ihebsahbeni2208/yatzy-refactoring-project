package com.dev.yatzy_efactoring.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by isahbeni on 6/3/2022.
 */
public class YatzyOperationsUtils {
    private YatzyOperationsUtils() {
        throw new IllegalStateException("Utility class");
    }
    public static List<Integer> retrievePairs(int d1, int d2, int d3, int d4, int d5, int occurrence) {
        List<Integer> pairDiceList = new ArrayList();
        for (Map.Entry mapEntry : getElementsFrequency(d1, d2, d3, d4, d5).entrySet()) {
            if ((int) mapEntry.getValue() >= occurrence) {
                pairDiceList.add((Integer)mapEntry.getKey());
            }
        }
        return pairDiceList;
    }
    public static Map<Integer, Integer> getElementsFrequency(int d1, int d2, int d3, int d4, int d5) {
        return Stream
                .of(d1, d2, d3, d4, d5)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.summingInt(e -> 1)));
    }
    public static boolean isDifferenceEqualToOneWhenSortedList(List <Integer> diceElements) {
        int differenceIsOne = 0;
        for (int i = 1; i <= diceElements.size() - 1; i++) {
            if (diceElements.get(i) - diceElements.get(i - 1) == 1) {
                differenceIsOne++;
            }
        }
        return differenceIsOne == 4;
    }
}
